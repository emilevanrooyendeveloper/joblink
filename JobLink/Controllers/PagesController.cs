﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JobLink.Controllers
{
    public class PagesController : Controller
    {
        public ActionResult Customer()
        {
            ViewBag.Message = "Your customer page.";
            return View();
        }

        public ActionResult InventoryItems()
        {
            ViewBag.Message = "Your inventory items page.";
            return View();
        }

        public ActionResult InvoiceManager()
        {
            ViewBag.Message = "Your invoice manager page.";
            return View();
        }

        public ActionResult InvoiceCreator()
        {
            ViewBag.Message = "Your invoice creator page.";
            return View();
        }
    }
}