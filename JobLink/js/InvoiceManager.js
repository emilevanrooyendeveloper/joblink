﻿$(function () {
    GetCustomers();
    GetInvoices();
});


function GetInvoices() {
    var html = '';
    html += '<tr>';
    html += '    <th>';
    html += '        Customer Name';
    html += '   </th>';
    html += '    <th>';
    html += '        Document Number';
    html += '    </th>';
    html += '    <th>';
    html += '        Total';
    html += '   </th>';
    html += '</tr>';

    $("#Invoices").html(html);
    $.ajax({
        type: "GET",
        url: localStorage.APIURI + '/api/invoice/Invoices' + '',
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            $.each(data, function (idx, ele) {

                var num = idx + 1;
                html += '<tr style="cursor:pointer" onclick="LoadInvoice(\'' + ele.id+'\')">';
                html += '    <td>' + ele.customerName + '</td>';
                html += '    <td>' + ele.documentNumber + '</td>';
                html += '    <td>' + ele.total + '</td>';
                html += '</tr>';

            });
            $("#Invoices").html(html);
        },
        error: function (request) {
            ActionResult(request);
        }
    });
}

function GetCustomers() {
    $.ajax({
        type: "GET",
        url: localStorage.APIURI + '/api/customer/Customers' + '',
        contentType: 'application/json',
        success: function (data) {
            $.each(data, function (idx, ele) {

                var html = '<option value="" disabled selected>Please Select</option>';
                $("#customerSelect").html(html);
                $.each(data, function (idx, ele) {
                    var num = idx + 1;
                    $("#customerSelect").append('<option value="' + ele.id + '"><p style=max-width:200px;>' + ele.customerName + '<p></option>');
                })

            });
        },
        error: function (request) {
            ActionResult(request);
        }
    });
}

function CreateInvoice() {
    var customer = {
        customerID: $("#customerSelect").val(),
        documentNumber: $("#DocumentNumber").val()
    }
    $.ajax({
        type: "post",
        url: localStorage.APIURI + '/api/invoice/insert',
        contentType: 'application/json',
        data: JSON.stringify(customer),
        success: function (data) {
            localStorage.InvoiceID = data.id;
            window.location = '/pages/invoicecreator';

        },
        error: function (request) {
            //ToDo: Show Error
        }
    });
}

function LoadInvoice(id) {
    localStorage.InvoiceID = id;
    window.location = '/pages/invoicecreator';
}