﻿$(function () {
    Cancel();
    GetItems();
});


function GetItems() {
    var html = '';
    html += '<tr>';
    html += '    <th>';
    html += '        Item Name';
    html += '   </th>';
    html += '    <th>';
    html += '        Unit Price';
    html += '   </th>';
    html += '    <th>';
    html += '        Action';
    html += '    </th>';
    html += '</tr>';

    $("#ExistingItems").html(html);
    $.ajax({
        type: "GET",
        url: localStorage.APIURI + '/api/inventory/Items' + '',
        contentType: 'application/json',
        success: function (data) {
            $.each(data, function (idx, ele) {

                var num = idx + 1;
                html += '<tr>';
                html += '    <td>' + ele.itemDescription + '</td>';
                html += '    <td>' + ele.unitPrice + '</td>';
                html += '    <td>';
                html += '        <button class="EditBtn" onclick="EditItem(\'' + ele.id + '\')">Edit</button>';
                html += '    </td>';
                html += '</tr>';

            });
            $("#ExistingItems").html(html);
        },
        error: function (request) {
            ActionResult(request);
        }
    });
}


function Add() {
    var item = {
        itemDescription: $("#ItemName").val(),
        unitPrice: $("#UnitPrice").val()
    }
    $.ajax({
        type: "post",
        url: localStorage.APIURI + '/api/inventory/insert',
        contentType: 'application/json',
        data: JSON.stringify(item),
        success: function (data) {
            Cancel();
            GetItems();
        },
        error: function (request) {
            //ToDo: Show Error
        }
    });
}

function EditItem(id) {
    $.ajax({
        type: "GET",
        url: localStorage.APIURI + '/api/inventory/Item?itemID=' + id + '',
        contentType: 'application/json',
        success: function (data) {

            var html = '';
            $("#ItemName").val(data.itemDescription);
            $("#UnitPrice").val(data.unitPrice);

            html += '<div class="row">';
            html += '    <div class="col">';
            html += '        <button class="SaveBtn" onclick="Save(\'' + id + '\')">Save</button>';
            html += '    </div>';
            html += '    <div class="col">';
            html += '        <button class="CancelBtn" onclick="Cancel()">Cancel</button>';
            html += '    </div>';
            html += '</div>';

            $("#ButtonArea").html(html);

        },
        error: function (request) {
            //ToDo: Show Error
        }
    });
}

function Cancel() {

    $("#ItemName").val('');
    $("#UnitPrice").val('');


    $("#ButtonArea").html('');
    $("#ButtonArea").html('<button class="SaveBtn" onclick="Add()">Add</button>');
}

function Save(id) {
    var item = {
        id: id,
        itemDescription: $("#ItemName").val(),
        unitPrice: $("#UnitPrice").val()
    }
    $.ajax({
        type: "post",
        url: localStorage.APIURI + '/api/inventory/update',
        contentType: 'application/json',
        data: JSON.stringify(item),
        success: function (data) {
            Cancel();
            GetItems();
        },
        error: function (request) {
            //ToDo: Show Error
        }
    });
}