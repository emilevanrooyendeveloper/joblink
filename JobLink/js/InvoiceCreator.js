﻿$(function () {
    GetItems();
    GetProducts();
});


function GetItems() {
    var html = '';
    html += '<tr>';
    html += '    <th>';
    html += '        Item Name';
    html += '            </th>';
    html += '    <th>';
    html += '        Quantity';
    html += '   </th>';
    html += '    <th>';
    html += '        Unit Price';
    html += '   </th>';
    html += '    <th>';
    html += '        Total';
    html += '   </th>';
    html += '    <th>';
    html += '        Action';
    html += '   </th>';
    html += '</tr>';

    $("#Items").html(html);
    $.ajax({
        type: "GET",
        url: localStorage.APIURI + '/api/invoiceitem/Items?invoiceID=' + localStorage.InvoiceID + '',
        contentType: 'application/json',
        success: function (data) {
            $.each(data, function (idx, ele) {

                var num = idx + 1;
                html += '<tr>';
                html += '    <td>' + ele.itemDescription + '</td>';
                html += '    <td>' + ele.quantity + '</td>';
                html += '    <td>' + ele.unitPrice + '</td>';
                var total = ele.quantity * ele.unitPrice;

                html += '    <td>' + total + '</td>';
                html += '    <td>';
                html += '        <button class="EditBtn" onclick="Remove(\'' + ele.id + '\')">Remove</button>';
                html += '    </td>';
                html += '</tr>';

            });
            $("#Items").html(html);
        },
        error: function (request) {
            ActionResult(request);
        }
    });
}

function GetProducts() {
    $.ajax({
        type: "GET",
        url: localStorage.APIURI + '/api/inventory/Items' + '',
        contentType: 'application/json',
        success: function (data) {
            $.each(data, function (idx, ele) {

                var html = '<option value="" disabled selected>Please Select</option>';
                $("#productSelect").html(html);
                $.each(data, function (idx, ele) {
                    var num = idx + 1;
                    $("#productSelect").append('<option value="' + ele.id + '"><p style=max-width:200px;>' + ele.itemDescription + '<p></option>');
                })

            });
        },
        error: function (request) {
            ActionResult(request);
        }
    });
}


function Add() {
    var item = {
        invoiceID: localStorage.InvoiceID,
        inventoryItemID: $("#productSelect").val(),
        quantity: $("#Quantity").val()
    }
    $.ajax({
        type: "post",
        url: localStorage.APIURI + '/api/invoiceitem/insert',
        contentType: 'application/json',
        data: JSON.stringify(item),
        success: function (data) {
            GetItems();
        },
        error: function (request) {
            //ToDo: Show Error
        }
    });
}

function Complete() {

    if (confirm('Are you sure you want to finish this invoice?')) {
        $.ajax({
            type: "GET",
            url: localStorage.APIURI + '/api/invoice/Calculate?invoiceID=' + localStorage.InvoiceID + '',
            contentType: 'application/json',
            success: function (data) {
                window.location = '/pages/invoicemanager';
            },
            error: function (request) {
                ActionResult(request);
            }
        });

    } else {
        // Do nothing!
    }
    
}