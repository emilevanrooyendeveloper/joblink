﻿$(function () {
    Cancel();
    GetCustomers();
});


function GetCustomers() {
    var html = '';
    html += '<tr>';
    html += '    <th>';
    html += '        Customer Name';
    html += '            </th>';
    html += '    <th>';
    html += '        Address';
    html += '            </th>';
    html += '    <th>';
    html += '        Contact Number';
    html += '            </th>';
    html += '    <th>';
    html += '        Action';
    html += '            </th>';
    html += '</tr>';

    $("#ExistingCustomers").html(html);
    $.ajax({
        type: "GET",
        url: localStorage.APIURI + '/api/customer/Customers' + '',
        contentType: 'application/json',
        success: function (data) {
            $.each(data, function (idx, ele) {

                var num = idx + 1;
                html += '<tr>';
                html += '    <td>' + ele.customerName+'</td>';
                html += '    <td>' + ele.address+'</td>';
                html += '    <td>' + ele.telephoneNumber+'</td>';
                html += '    <td>';
                html += '        <button class="EditBtn" onclick="EditCustomer(\''+ele.id+'\')">Edit</button>';
                html += '    </td>';
                html += '</tr>';

            });
            $("#ExistingCustomers").html(html);
        },
        error: function (request) {
            ActionResult(request);
        }
    });
}


function Add() {
    var customer = {
        customerName: $("#CustomerName").val(),
        address: $("#Address").val(),
        telephoneNumber: $("#TelephoneNumber").val()
    }
    $.ajax({
        type: "post",
        url: localStorage.APIURI + '/api/customer/insert',
        contentType: 'application/json',
        data: JSON.stringify(customer),
        success: function (data) {
            Cancel();
            GetCustomers();
        },
        error: function (request) {
            //ToDo: Show Error
        }
    });
}

function EditCustomer(id) {
    $.ajax({
        type: "GET",
        url: localStorage.APIURI + '/api/customer/Customer?customerID=' + id + '',
        contentType: 'application/json',
        success: function (data) {

            var html = '';
            $("#CustomerName").val(data.customerName);
            $("#Address").val(data.address);
            $("#TelephoneNumber").val(data.telephoneNumber);



            html += '<div class="row">';
            html += '    <div class="col">';
            html += '        <button class="SaveBtn" onclick="Save(\'' + id +'\')">Save</button>';
            html += '    </div>';
            html += '    <div class="col">';
            html += '        <button class="CancelBtn" onclick="Cancel()">Cancel</button>';
            html += '    </div>';
            html += '</div>';

            $("#ButtonArea").html(html);

        },
        error: function (request) {
            //ToDo: Show Error
        }
    });
}

function Cancel() {

    $("#CustomerName").val('');
    $("#Address").val('');
    $("#TelephoneNumber").val('');


    $("#ButtonArea").html('');
    $("#ButtonArea").html('<button class="SaveBtn" onclick="Add()">Add</button>');
}

function Save(id) {
    var customer = {
        id: id,
        customerName: $("#CustomerName").val(),
        address: $("#Address").val(),
        telephoneNumber: $("#TelephoneNumber").val()
    }
    $.ajax({
        type: "post",
        url: localStorage.APIURI + '/api/customer/update',
        contentType: 'application/json',
        data: JSON.stringify(customer),
        success: function (data) {
            Cancel();
            GetCustomers();
        },
        error: function (request) {
            //ToDo: Show Error
        }
    });
}